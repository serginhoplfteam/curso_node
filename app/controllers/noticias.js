'use strict';

module.exports.noticias = function(application, req, res){

	let connection = application.config.dbConnection();
	let noticiasDAO = new application.app.models.NoticiasDAO(connection);

	noticiasDAO.getNoticias(function(error, result){
		res.render("noticias/noticias", {noticias: result});
	})
}

module.exports.noticia = function(application, req, res){
	let connection = application.config.dbConnection();
	let noticiasDAO = new application.app.models.NoticiasDAO(connection);

	noticiasDAO.getNoticia(function(error, result){
		res.render("noticias/noticia", {noticia: result});
	});
}
