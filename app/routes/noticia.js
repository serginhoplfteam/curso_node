'use strict';

module.exports = function(application){

	application.get('/noticia', function(req, res){

		let connection = application.config.dbConnection();
		let noticiasDAO = new application.app.models.NoticiasDAO(connection);

		noticiasDAO.getNoticia(function(error, result){
			res.render("noticias/noticia", {noticia: result});
		});
	});
}