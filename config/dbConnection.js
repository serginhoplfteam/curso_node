'use strict';

const mysql = require('mysql');

let connMySQL = function(){
	return mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: 'root',
		database: 'portal_noticias'
	});
}

module.exports = function () {
	return connMySQL;
}